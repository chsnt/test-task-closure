/**
 * 3) Как сделать так, что "finish" выводился после последнего числа?
 * Оборачиваем setTimeout в промисы, закидываем их в массив, исполняем их и ждем окончания при помощи Promise.all,
 * затем выводим 'finish'.
 */

const start = Date.now();
let arr = [];
for (let i = 0; i < 5; i++) {
    arr.push(
        new Promise((resolve) => {
            setTimeout(() => {
                console.log(i, Date.now() - start);
                resolve();
            }, i * 1000);
        })
    );
}

Promise.all(arr).then(() => console.log('finish', Date.now() - start));
