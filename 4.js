/**
 * 4) Изменится ли решение, если вместо "i * 1000" написать "Math.random() * 1000"?
 * Результ изменится в связи с изменением значения задержки setTimeout. Ранее оно было согласовано со значением
 * итератора i и было прямопропорционально нему. После изменения же согласованность исчезла и задержка "отвязалась"
 * от итератора и является случайным числом.
 */

const start = Date.now();
let arr = [];
for (let i = 0; i < 5; i++) {
    arr.push(
        new Promise((resolve) => {
            setTimeout(() => {
                console.log(i, Date.now() - start);
                resolve();
            }, Math.random() * 1000);
        })
    );
}

Promise.all(arr).then(() => console.log('finish', Date.now() - start));
