/** Дан код: */
const start = Date.now();
for (var i = 0; i < 5; i++) {
    setTimeout(() => {
        console.log(i, Date.now() - start);
    }, i * 1000);
}

console.log('finish', Date.now() - start);
